#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {
    ofBackground(100,100,100);

    w = 320;
    h = 240;
    guiWidth = 210;

    movie.initGrabber(w, h);

    //reserve memory for cv images
    rgb.allocate(w, h);
    red.allocate(w, h);
    green.allocate(w, h);
    blue.allocate(w, h);
    filtered.allocate(w,h);
    memory.allocate(w,h);
    path.allocate(w,h,OF_IMAGE_GRAYSCALE);

    myfont.loadFont("arial.ttf",10);

    //GUI
    guiColor.setup("Color", "Color_settings.xml", 0, h+10);
    guiColor.add(toggleColor.setup("Color on/off", false));
    guiColor.add(sliderR.setup("Rcoeef", 0.0, -10.0, 10.0));
    guiColor.add(sliderG.setup("Gcoeff", 0.0, -10.0, 10.0));
    guiColor.add(sliderB.setup("Bcoeff", 0.0, -10.0, 10.0));

    guiThreshold.setup("Threshold", "Threshold_settings.xml", guiWidth, h+10);
    guiThreshold.add(toggleThreshold.setup("Threshold on/off", false));
    guiThreshold.add(sliderThreshold.setup("Threshold", 100, 0, 255));

    guiContour.setup("Contour finder", "Contour_settings.xml", guiWidth*2, h+10);
    guiContour.add(labelContourCount.setup("Contours found",""));
    guiContour.add(areaLow.setup("Area low", 2500, 0, w*h/8));
    guiContour.add(areaHigh.setup("Area high", w*h/4, 0, w*h/2));
    guiContour.add(sliderBlobCount.setup("Max contours", 5, 0, 50));
    guiContour.add(toggleContourHoles.setup("Find contour holes", false));

    guiMemory.setup("Memory", "Memory_settings.xml", guiWidth*3, h+10);
    guiMemory.add(buttonRemember.setup("Remember"));
    guiMemory.add(toggleAbsdiff.setup("Absdiff on/off", false));

    guiMethod.setup("Methods", "Method_settings.xml", guiWidth*4, h+10);
    guiMethod.add(toggleMethod1.setup("Method 1", false));
    guiMethod.add(toggleMethod2.setup("Method 2", false));
    guiMemory.add(resetPath.setup("Reset path image"));

    guiImage.setup("Image control", "Image_settings.xml", guiWidth*3, h+150);
    guiImage.add(toggleContrastStretch.setup("Stretch contrast", true));
    guiImage.add(toggleMirrorHorizontal.setup("Mirror Vertical", false));
    guiImage.add(toggleMirrorVertical.setup("Mirror Hotizontal", false));
    guiImage.add(toggleBlur.setup("Blur on/off", false));
    guiImage.add(sliderBlur.setup("Blur", 21, 0, 50));

    pathColor.set(255,85);

    for(int i=0; i<w*h; i++){
       path.setColor(i,0);
    }
    path.update();
    //path.setFromPixels(pathPixels);



}

//--------------------------------------------------------------
void testApp::update(){

    movie.update();

    if (movie.isFrameNew()) {

        rgb.setFromPixels(movie.getPixels(), w, h);


        if(toggleMirrorVertical || toggleMirrorHorizontal){
           rgb.mirror(toggleMirrorVertical, toggleMirrorHorizontal);
        }

    if (sliderBlur%2 != 1){
        sliderBlur = sliderBlur+1;
    }

        if(toggleMethod1){

            if(toggleColor){

                rgb.convertToGrayscalePlanarImages(red, green, blue);
                for(int i=0; i<w*h; i++){
                    //Add rgb to each other with coeff
                    filteredValue = red.getPixels()[i]*sliderR + green.getPixels()[i]*sliderG + blue.getPixels()[i]*sliderB;
                    if(filteredValue > 255){
                        filtered.getPixels()[i] = 255;
                    }
                    else if (filteredValue < 0){
                        filtered.getPixels()[i] = 0;
                    }
                    else {
                        filtered.getPixels()[i] = filteredValue;
                    }
                }
            } else {
                rgb.convertToGrayscalePlanarImage(filtered,0);
            }

            if(buttonRemember){
                memory=filtered;
            }

            if(toggleAbsdiff){
                filtered.absDiff(memory);
            }

            if(toggleContrastStretch){
            filtered.contrastStretch();
            }

            if(toggleThreshold){
                filtered.threshold(sliderThreshold);
            }

            if(toggleBlur){
                filtered.blur(sliderBlur);
            }

        }

        if(toggleMethod2){

            rgb.convertToGrayscalePlanarImage(filtered,0);
            if(toggleContrastStretch){
            //Stretch contrast
            filtered.contrastStretch();
            }

            if(toggleThreshold){
                filtered.threshold(sliderThreshold);
            }

            if(buttonRemember){
                memory = filtered;
            }

            if(toggleAbsdiff){
                filtered.absDiff(memory);
            }

            if(toggleBlur){
                filtered.blur(sliderBlur);
            }

        }

        filtered.flagImageChanged();

        //run the contour finder on the filtered image to find blobs
        contours.findContours(filtered, areaLow, areaHigh, sliderBlobCount, toggleContourHoles, true);

        labelContourCount = ofToString(contours.nBlobs);
    }

    if(resetPath){
        for(int i=0; i<w*h; i++){
            path.setColor(i,0);
        }
        path.update();
    }
}

//--------------------------------------------------------------
void testApp::draw(){

    myfont.drawString("Method 1: Color->Absdiff->Contrast stretch->Threshold->Blur\nMethod 2: Threshold->Absdiff->Blur", 10,520);

    guiColor.draw();
    guiContour.draw();
    guiMemory.draw();
    guiThreshold.draw();
    guiMethod.draw();
    guiImage.draw();

    //Draw Rgb image
    rgb.draw(0,0);
    filtered.draw(w,0);
    path.draw(w*2,0);


    ofEnableAlphaBlending();

    //Get all blobs
    for(int i = 0; i < contours.nBlobs; i++) {
        //Get blobs
        ofxCvBlob blob = contours.blobs.at(i);

        //Draw center circle
        //ofCircle(blob.centroid.x, blob.centroid.y, 3);
        ofSetColor(pathColor);

        ofCircle(blob.centroid.x+2*w, blob.centroid.y, 5);
        path.setColor(blob.centroid.x , blob.centroid.y, pathColor);

        //Draw Bounding box
        ofRect(blob.boundingRect);

        ofSetColor(255,255,0,85);

        ofFill();
        //Draw blob shaope
        ofBeginShape();
        ofVertices(blob.pts);
        ofEndShape();

        ofNoFill();

        ofSetColor(255,255,255);

        sprintf(blobPos,"X: %g\nY: %g\nArea: %g", round(blob.centroid.x), round(blob.centroid.y), round(blob.area));
        myfont.drawString(blobPos,blob.centroid.x+10, blob.centroid.y);
        //myfont.drawString(ofToString(blob.centroid.x),blob.centroid.x+10, blob.centroid.y);

    }

    path.update();

    ofDisableAlphaBlending();
    ofSetColor(255,255,255);


}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button) {

}
