#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxGUI.h"
#include <vector>

class testApp : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    void mousePressed(int x, int y, int button);

    //of classes
    ofVideoGrabber movie;
    ofPixels pixelsR, pixelsG, pixelsB;

    //Opencv classes
    ofxCvColorImage rgb;
    ofxCvGrayscaleImage filtered,memory,red,green,blue;
    ofxCvContourFinder contours;

    //Gui Color
    ofxPanel guiColor;
    ofxFloatSlider sliderR, sliderG, sliderB;
    ofxIntSlider sliderColorThreshold;
    ofxToggle toggleColor;

    //Gui Thresholf
    ofxPanel guiThreshold;
    ofxIntSlider sliderThreshold;
    ofxToggle toggleThreshold;

    //Gui Contour
    ofxPanel guiContour;
    ofxIntSlider areaLow, areaHigh, sliderBlobCount;
    ofxToggle toggleContourHoles;
    ofxLabel labelContourCount;

    //Gui Memory
    ofxPanel guiMemory;
    ofxButton buttonRemember;
    ofxToggle toggleAbsdiff;

    //Gui Methods
    ofxPanel guiMethod;
    ofxToggle toggleMethod1, toggleMethod2;
    ofxButton resetPath;

    //Gui Image
    ofxPanel guiImage;
    ofxToggle toggleContrastStretch, toggleMirrorHorizontal, toggleMirrorVertical, toggleBlur;
    ofxIntSlider sliderBlur;


    //variables;
    int w, h, guiWidth, *pointer;
    float filteredValue;
    bool invert;
    char blobPos[30];
    unsigned char *pathValue;
    ofImage path;
    ofColor pathColor;

    ofTrueTypeFont myfont;

    cv::Mat originalIn, originalOut;
};

#endif
